# Changelog

## 1.1.2

**Changes:**
* Fix running PhoToRectify with Python version lower than 3.8.

## 1.1.1

**Changes:**
* Added License.
* Minor bug fixes.

## 1.1.0

**Changes:**
* Switched from setup.py to poetry.
* The `install` script is deprecated.
* New versions will be available on [pypi](https://pypi.org/project/photorectify).
* Switched to Semantic Versioning.
* Renamed project to PhoToRectify.

## 1.0.0.0 (2020-06-05)

**Implemented enhancements:**
* Added multiprocessing.
* Set default export format to TIFF w/ alpha channel. JPEG is still available by option `--jpg`.
* Added support for importing also only one photo.
* Allow optional recursively processing of photos.
* Added option for specifing an output directory.
* Added option for specifying a custom suffix for export filename.
* Added option for overwriting mandatory possible EXIF tags.
* Allow use of custom lensfun database (format: XML).
* Enhanced copying of EXIF tags (try all possible keys) and also option to disable EXIF copying.
